# README #

Fundio - A Crowd Funding website powered by Django

### What is this repository for? ###

* Code base for Fundio

### How do I get set up? ###

* Dependencies  
```sh
        Python(3.x)
        Django
        MySQL
```

* Deployment instructions  
```sh
     $ pip install djangorestframework
     $ pip install django-rest-swagger
     $ pip install mysqlclient

Inside fundio main folder run the following commands -
     $ python manage.py migrate
     $ python manage.py startserver

Note: You can use virtualenv to avoid python packages conflict

Hit localhost:8000/ in your web browser to get the swagger UI containing the APIs -

```       
![Alt text](/swagger.PNG?raw=true "Optional Title")

### Who do I talk to? ###

* saurabh.joshi25@gmail.com
* bhupendrasingh.suyal@gmail.com