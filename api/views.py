from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth import hashers,authenticate, login ,logout
from django.http import Http404

from rest_framework import status
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.authtoken.views import ObtainAuthToken


from rest_framework import authentication
from rest_framework import exceptions


# from api.models import User
from django.contrib.auth.models import User
from api.serializers import UserSerializer


class LogoutUser(generics.GenericAPIView):

    def get(self, request, format=None):
        logout(request)
        return Response('Successful logout!',status=status.HTTP_201_CREATED)
    # Redirect to a success page.

class UserList(generics.GenericAPIView):
    """
    get:
    Return all users

    post:
    Create new user
    """
    serializer_class = UserSerializer
    def get(self, request, format=None):
        api = User.objects.all()
        serializer = UserSerializer(api, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class UserAuthentication(ObtainAuthToken,generics.GenericAPIView):
    """
    post:
    Authenticate a user
    """
    def post(self, request, format=None):
        username = request.data.get('username')
        password = request.data.get('password')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            # Redirect to a success page.
            return Response('Successful login!',status=status.HTTP_201_CREATED)
        else:
            return Response('Failure',status=status.HTTP_400_BAD_REQUEST)
        # Return an 'invalid login' error message.


class UserDetail(generics.GenericAPIView):
    """
    get:
    Get user

    put:
    Edit user info

    delete:
    Remove user
    """
    serializer_class = UserSerializer
    def get_object(self, pk):
        try:
            return User.objects.get(pk=pk)
        except User.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        User = self.get_object(pk)
        serializer = UserSerializer(User)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        User = self.get_object(pk)
        serializer = UserSerializer(User, data=request.data)
        if serializer.is_valid():
            serializer.save(password=hashers.make_password(request.data['password']))
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        User = self.get_object(pk)
        User.delete()
        return Response({'User deleted successfully!'},status=status.HTTP_200_OK)