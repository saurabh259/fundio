
from rest_framework import serializers
# from backendAPIs.models import User
from django.contrib.auth.models import User


class UserSerializer(serializers.ModelSerializer):
	class Meta:
	    model = User
	    fields = ('id','first_name', 'last_name', 'email', 'password','username')

	def create(self, validated_data):
		user = User(
		    email=validated_data['email'],
		    username=validated_data['username']
		)
		user.set_password(validated_data['password'])
		user.save()
		return user
